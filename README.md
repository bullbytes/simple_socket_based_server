# What is this?
A minimal web server that can serve an image and a text response via HTTPS or HTTP.

The server builds on Java's [ServerSocket](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/net/ServerSocket.html) directly for responding to client requests. It has no dependencies but the JDK.

[Here's](https://gitlab.com/bullbytes/simple_socket_based_server/blob/master/src/main/java/com/bullbytes/simpleserver/Start.java#L70) the server's main request-handling loop.

## Prerequisites
* JDK 13 or later since [there's a bug](https://stackoverflow.com/questions/57679669/ssl-error-rx-record-too-long-with-custom-server/57868603) in the JDK's TLS implementation in prior versions.

# Build instructions
To start the server with TLS enabled:

    ./gradlew run

To start the server without TLS:

    ./gradlew run --args="--use-tls=no"

# Get a server response
Use

    curl -k https://localhost:8443/

to get a textual response from the server.

Do

    curl -Ok https://localhost:8443/ada.jpg

to download an image from the server.
