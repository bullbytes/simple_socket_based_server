package com.bullbytes.simpleserver;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

/**
 * Starts the server.
 * <p>
 * Person of contact: Matthias Braun
 */
public enum Start {
    ;

    // Used to read from and write to the socket's input and output streams
    private static final Charset ENCODING = StandardCharsets.UTF_8;
    private static final String NO_TLS_ARG = "--use-tls=no";

    /**
     * Starts our server, ready to handle requests.
     *
     * @param args if the arguments contain {@value NO_TLS_ARG}, the server will respond via HTTP.
     *             Otherwise, it'll use HTTPS
     */
    public static void main(String... args) {
        var address = new InetSocketAddress("0.0.0.0", 8443);

        boolean useTls = shouldUseTls(args);

        startServer(address, useTls);
    }

    private static boolean shouldUseTls(String[] args) {
        boolean useTls = true;

        for (String arg : args) {
            if (arg.equals(NO_TLS_ARG)) {
                useTls = false;
                break;
            }
        }
        return useTls;
    }

    public static void startServer(InetSocketAddress address, boolean useTls) {

        String enabledOrDisabled = useTls ? "enabled" : "disabled";
        System.out.println(format("Starting server at %s with TLS %s", address, enabledOrDisabled));

        try (var serverSocket = useTls ?
                getSslSocket(address) :
                // Create a server socket without TLS
                new ServerSocket(address.getPort(), 0, address.getAddress())) {

            // This infinite loop is not CPU-intensive per se since method "accept" blocks
            // until a client has made a connection to the socket's port
            while (true) {
                try (var socket = serverSocket.accept();
                     // Read the client's request from the socket
                     var requestStream = new BufferedReader(new InputStreamReader(socket.getInputStream(), ENCODING));
                     // The server writes its response to the socket's output stream
                     var responseStream = new BufferedOutputStream(socket.getOutputStream())
                ) {
                    System.out.println("Accepted connection on " + socket);

                    String requestedResource = getRequestedResource(requestStream)
                            .orElse("unknown");

                    byte[] response = requestedResource.equals("/ada.jpg") ?
                            getJpgResponse(new URL("https://upload.wikimedia.org/wikipedia/commons/a/a4/Ada_Lovelace_portrait.jpg")) :
                            getTextResponse("The server says hi 👋", StatusCode.SUCCESS);

                    responseStream.write(response);

                    // It's important to flush the response stream before closing it to make sure any
                    // unsent bytes in the buffer are sent via the socket. Otherwise, the client gets an
                    // incomplete response
                    responseStream.flush();
                } catch (IOException e) {
                    System.err.println("Exception while handling connection");
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.err.println("Could not create socket at " + address);
            e.printStackTrace();
        }
    }

    private static Optional<String> getRequestedResource(BufferedReader requestStream) {
        var lines = getHeaderLines(requestStream);

        return first(lines).map(statusLine -> {
            // Go past the space
            int beginIndex = statusLine.indexOf(' ') + 1;
            int endIndex = statusLine.lastIndexOf(' ');
            return statusLine.substring(beginIndex, endIndex);
        });
    }

    private static <E> Optional<E> first(List<? extends E> list) {
        return (list != null && !list.isEmpty()) ?
                Optional.ofNullable(list.get(0)) :
                Optional.empty();
    }

    private static List<String> getHeaderLines(BufferedReader reader) {

        var headerLines = new ArrayList<String>();
        try {
            var line = reader.readLine();
            // The header is concluded when we see an empty line.
            // The line is null if the end of the stream was reached without reading
            // any characters. This can happen if the client tries to connect with
            // HTTPS while the server expects HTTP
            while (line != null && !line.isEmpty()) {
                headerLines.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            System.err.println("Could not read all lines from request");
            e.printStackTrace();
        }
        return headerLines;
    }

    private static ServerSocket getSslSocket(InetSocketAddress address)
            throws Exception {

        // Backlog is the maximum number of pending connections on the socket, 0 means an
        // implementation-specific default is used
        int backlog = 0;

        var keyStorePath = Path.of("./tls/keystore.jks");
        char[] keyStorePassword = "pass_for_self_signed_cert".toCharArray();

        // Bind the socket to the given port and address
        var serverSocket = getSslContext(keyStorePath, keyStorePassword)
                .getServerSocketFactory()
                .createServerSocket(address.getPort(), backlog, address.getAddress());

        // We don't need the password anymore → Overwrite it
        Arrays.fill(keyStorePassword, '0');

        return serverSocket;
    }

    private static SSLContext getSslContext(Path keyStorePath, char[] keyStorePassword)
            throws Exception {

        var keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream(keyStorePath.toFile()), keyStorePassword);

        var keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
        keyManagerFactory.init(keyStore, keyStorePassword);

        var sslContext = SSLContext.getInstance("TLS");
        // Null means using default implementations for TrustManager and SecureRandom
        sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
        return sslContext;
    }

    private static byte[] concat(byte[] first, byte[] second) {
        // New array with contents of first one, having the length of the two input arrays combined
        byte[] result = Arrays.copyOf(first, first.length + second.length);
        // Copy the second array into the result array starting at the end of the first array
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    private static byte[] getJpgResponse(URL fileUrl) {

        byte[] response;
        try (var fileStream = fileUrl.openStream()) {
            var imageBytes = fileStream.readAllBytes();
            var fileName = new File(fileUrl.getPath()).getName();

            System.out.println(format("Sending image '%s' with %d bytes in response", fileName, imageBytes.length));

            var statusLine = "HTTP/1.1 200 OK";
            var contentLength = "Content-Length: " + imageBytes.length;
            var contentType = "Content-Type: image/jpeg";
            var contentDisposition = format("Content-Disposition: inline; filename=%s", fileName);

            String header = statusLine + "\r\n" +
                    contentLength + "\r\n" +
                    contentType + "\r\n" +
                    contentDisposition + "\r\n" +
                    "\r\n";

            // Append the bytes of the image to the bytes of the header
            response = concat(header.getBytes(ENCODING), imageBytes);

        } catch (IOException e) {
            var msg = format("Could not read file at URL '%s'", fileUrl);
            System.err.println(msg);
            response = getTextResponse(msg, StatusCode.SERVER_ERROR);
        }
        return response;
    }

    private static byte[] getTextResponse(String text, StatusCode status) {
        var body = text + "\r\n";
        var contentLength = body.getBytes(ENCODING).length;
        var statusLine = format("HTTP/1.1 %s %s\r\n", status.code, status.text);

        var response = statusLine +
                format("Content-Length: %d\r\n", contentLength) +
                format("Content-Type: text/plain; charset=%s\r\n",
                        ENCODING.displayName()) +
                "\r\n" +
                body;

        return response.getBytes(ENCODING);
    }

    /**
     * HTTP status codes such as 200 and 500.
     * <p>
     * Person of contact: Matthias Braun
     */
    public enum StatusCode {
        SUCCESS(200, "Success"),
        SERVER_ERROR(500, "Internal Server Error");

        private final int code;
        private final String text;

        StatusCode(int code, String text) {
            this.text = text;
            this.code = code;
        }

        /**
         * @return "200 Success" or "500 Internal Server Error", for example
         */
        @Override
        public String toString() {
            return code + " " + text;
        }
    }
}
